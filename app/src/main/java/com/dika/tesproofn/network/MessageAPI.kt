package com.dika.tesproofn.network

import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.arch.model.MessageResponse
import com.dika.tesproofn.arch.model.SendPayload
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface MessageAPI {
    @GET("messages/inbox")
    fun getMessages(): Observable<MessageResponse>

    @POST("messages/inbox/{id}")
    @FormUrlEncoded
    fun getMessage(@Path("id") id: Int): Observable<Message>

    @POST("messages/inbox/{id}/trash")
    @FormUrlEncoded
    fun deleteMessage(@Path("id") id: Int): Observable<ResponseBody>

    @POST("messages/send")
    fun sendMessage(@Body message: SendPayload): Observable<Message>
}