package com.dika.tesproofn.network

import com.dika.tesproofn.arch.model.Session
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthAPI {
    @POST("auth/login")
    @FormUrlEncoded
    fun login(
            @Field("identifier") identifier: String,
            @Field("password") password: String
    ): Observable<Session>
}