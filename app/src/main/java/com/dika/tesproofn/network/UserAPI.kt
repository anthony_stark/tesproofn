package com.dika.tesproofn.network

import com.dika.tesproofn.arch.model.User
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.*

interface UserAPI {
    @POST("user/profile")
    @FormUrlEncoded
    fun getProfile(): Observable<User>

    @PUT("user/profile")
    fun updateProfile(): Observable<User>

    @POST("user/avatar")
    @FormUrlEncoded
    fun updateAvatar(@Part("avatar") img: Multipart): Observable<ResponseBody>
}