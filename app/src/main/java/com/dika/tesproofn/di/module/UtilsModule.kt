package com.dika.tesproofn.di.module

import android.app.Application
import com.dika.tesproofn.utils.PreferenceUtils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilsModule(private var context: Application) {
    @Singleton
    @Provides
    internal fun sp(): PreferenceUtils {
        return PreferenceUtils(context)
    }
}