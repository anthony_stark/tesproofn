package com.dika.tesproofn.di.module

import android.app.Application
import com.dika.tesproofn.BuildConfig
import com.dika.tesproofn.network.AuthAPI
import com.dika.tesproofn.network.MessageAPI
import com.dika.tesproofn.network.UserAPI
import com.dika.tesproofn.utils.retrofit.OfflineCacheInterceptor
import com.dika.tesproofn.utils.retrofit.OnlineCacheInterceptor
import com.dika.tesproofn.utils.retrofit.TokenAuthenticator
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.reactivex.schedulers.Schedulers
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

@Module
object NetworkModule {
    private const val DISK_CACHE_SIZE: Long = 10 * 1024 * 1024 //10 MB

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideAuthenticator(context: Application): TokenAuthenticator {
        return TokenAuthenticator(context)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideCacheInterceptor(): OnlineCacheInterceptor {
        return OnlineCacheInterceptor()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideOfflineCacheInterceptor(context: Application): OfflineCacheInterceptor {
        return OfflineCacheInterceptor(context)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideOkHttpCache(context: Application): Cache {
        return Cache(context.cacheDir, DISK_CACHE_SIZE)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideOkHttpClient(
            logging: HttpLoggingInterceptor,
            cache: Cache,
            authenticator: TokenAuthenticator,
            onlineCacheInterceptor: OnlineCacheInterceptor,
            offlineCacheInterceptor: OfflineCacheInterceptor
    ): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.cache(cache)
        client.addInterceptor(logging)
        client.addInterceptor(offlineCacheInterceptor)
        client.authenticator(authenticator)
        client.addNetworkInterceptor(onlineCacheInterceptor)
        client.connectTimeout(30, TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        client.writeTimeout(30, TimeUnit.SECONDS)
        return client.build()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideAuthApi(retrofit: Retrofit): AuthAPI {
        return retrofit.create(AuthAPI::class.java)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideUserApi(retrofit: Retrofit): UserAPI {
        return retrofit.create(UserAPI::class.java)
    }

    @Provides
    @Reusable
    @JvmStatic
    internal fun provideMessageApi(retrofit: Retrofit): MessageAPI {
        return retrofit.create(MessageAPI::class.java)
    }
}