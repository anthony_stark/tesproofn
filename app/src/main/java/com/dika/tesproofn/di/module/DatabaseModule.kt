package com.dika.tesproofn.di.module

import android.app.Application
import androidx.room.Room
import com.dika.tesproofn.BuildConfig
import com.dika.tesproofn.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(context: Application): AppDatabase {
        return Room
                .databaseBuilder(context, AppDatabase::class.java, BuildConfig.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}