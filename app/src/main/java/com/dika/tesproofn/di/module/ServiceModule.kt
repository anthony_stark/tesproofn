package com.dika.tesproofn.di.module

import com.dika.tesproofn.service.UploadService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceModule {
    @ContributesAndroidInjector
    internal abstract fun uploadService(): UploadService
}