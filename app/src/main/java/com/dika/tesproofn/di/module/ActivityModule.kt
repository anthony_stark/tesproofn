package com.dika.tesproofn.di.module

import com.dika.tesproofn.arch.ui.home.HomeActivity
import com.dika.tesproofn.arch.ui.login.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun loginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun homeActivity(): HomeActivity

}