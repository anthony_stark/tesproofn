package com.dika.tesproofn.di.component

import android.app.Application
import com.dika.tesproofn.base.MyApp
import com.dika.tesproofn.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@SuppressWarnings()
@Component(
        modules = [
            AndroidInjectionModule::class,
            ActivityModule::class,
            FragmentModule::class,
            ServiceModule::class,
            NetworkModule::class,
            DatabaseModule::class,
            ViewModelModule::class,
            UtilsModule::class
        ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun network(module: NetworkModule): Builder
        fun database(module: DatabaseModule): Builder
        fun utils(module: UtilsModule): Builder
        fun build(): AppComponent
    }

    fun inject(app: MyApp)
}