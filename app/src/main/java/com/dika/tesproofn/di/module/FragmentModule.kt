package com.dika.tesproofn.di.module

import com.dika.tesproofn.arch.ui.home.InboxFragment
import com.dika.tesproofn.arch.ui.profile.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun mainFragment(): InboxFragment

    @ContributesAndroidInjector
    internal abstract fun profileFragment(): ProfileFragment
}