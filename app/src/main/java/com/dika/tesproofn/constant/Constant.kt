package com.dika.tesproofn.constant

object Constant {
    const val KEY_HEADER_ACCESS_TOKEN = "Authorization"
    const val KEY_HEADER_CACHE_CONTROL = "Cache-Control"
}