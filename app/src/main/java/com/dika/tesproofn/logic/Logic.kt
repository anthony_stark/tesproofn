package com.dika.tesproofn.logic

import android.util.Log

object Logic {
    fun check(x: Int): Boolean {
        val a = arrayOf(9, 10, 8, 4, 5, 3)
        a.forEachIndexed { i, it ->
            val m = x % it
            Log.d("two sum", "" + m)
            a.forEachIndexed { j, it2 ->
                if (j != i && m == it2) {
                    return true
                }
            }
        }
        return false
    }

    fun maxSubArray() {
        val a = arrayOf(1, -3, 5, -2, 9, -8, -6, 4)
        var maxVal = 0
        var sum = 0
        var start = 0
        var end = 0
        a.forEachIndexed { i, it ->
            sum += it
            if (it > maxVal) {
                maxVal = it
                end = i
            }
            if (sum < 0) {
                sum = 0
                start++
            }
        }
        Log.d("max sub array", a.filterIndexed { index, _ -> index in start..end }.toString())
    }

    fun div(dividen: Int, divisor: Int): Double {
        var isDecimal = false
        var dvd = dividen
        var digit = 10
        var decimal = 0.1
        if (dividen < divisor) {
            while (divisor > digit) {
                digit *= 10
                decimal *= 0.1
            }
            dvd *= digit
            isDecimal = true
        }
        if (dvd == divisor) return 1.0
        var count = 0.0
        while (dvd >= divisor) {
            dvd -= divisor
            count++
        }
        return if (isDecimal) {
            count * decimal
        } else {
            count
        }
    }

    fun coinChange(d: Array<Int>, n: Int) {
        val c = mutableListOf<Int>()
        val s = mutableListOf<Int>()
        val v = mutableListOf<Int>()
        for (i in 0..n) {
            if (i == 0) {
                c += 0
                s += 0
            } else {
                var min = Int.MAX_VALUE
                var coin = 0
                for (j in 0..(d.size - 1)) {
                    if (d[j] <= i) {
                        if (1 + c[i - d[j]] < min) {
                            min = 1 + c[i - d[j]]
                            coin = j + 1
                        }
                    }
                }
                c += min
                s += coin
            }
        }
        var a = n
        while (a > 0) {
            v += d[s[a] - 1]
            a -= d[s[a] - 1]
        }


        Log.d("coin change", "required: " + c[n])
        Log.d("coin change", "coin set: " + v.toString())
    }
}