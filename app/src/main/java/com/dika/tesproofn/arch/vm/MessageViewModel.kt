package com.dika.tesproofn.arch.vm

import androidx.annotation.WorkerThread
import androidx.lifecycle.MutableLiveData
import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.base.BaseViewModel
import com.dika.tesproofn.db.AppDatabase
import com.dika.tesproofn.network.MessageAPI
import com.dika.tesproofn.utils.ErrorParser
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MessageViewModel @Inject constructor() : BaseViewModel() {
    @Inject
    lateinit var messageAPI: MessageAPI
    @Inject
    lateinit var db: AppDatabase

    private var subscription: Disposable? = null

    var messages: MutableLiveData<List<Message>> = MutableLiveData()

    fun getMessages() {
        subscription = Observable.fromCallable { db.messageDAO().gets() }
                .concatMap { dataDb ->
                    if (dataDb.isEmpty()) {
                        messageAPI.getMessages().concatMap { dataApi ->
                            insertMessages(dataApi.data)
                            Observable.just(dataApi.data)
                        }
                    } else {
                        Observable.just(dataDb)
                    }
                }
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { showLoading() }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { hideLoading() }
                .subscribe(
                        { onMessagesResult(it) },
                        {
                            Timber.e(it)
                            showError(ErrorParser.getErrorMessage(it))
                        }
                )
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }

    private fun onMessagesResult(messages: List<Message>) {
        Timber.d(messages.toString())
        this.messages.value = messages
    }

    @WorkerThread
    private fun insertMessages(messages: List<Message>) {
        db.messageDAO().clear()
        db.messageDAO().insertAll(messages)
    }
}