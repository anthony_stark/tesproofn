package com.dika.tesproofn.arch.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class User(
        @PrimaryKey
        @Json(name = "id")
        var id: Int,
        @Json(name = "firstName")
        var firstName: String?,
        @Json(name = "fullName")
        var fullName: String?,
        @Json(name = "avatarPathSmall")
        var avatarSmall: String?,
        @Json(name = "avatarPathMedium")
        var avatarMedium: String?,
        @Json(name = "avatarPathLarge")
        val avatarLarge: String?,
        @Json(name = "email")
        val email: String?,
        @Json(name = "username")
        val username: String?
)