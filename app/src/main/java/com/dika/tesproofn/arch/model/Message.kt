package com.dika.tesproofn.arch.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@Entity
@JsonClass(generateAdapter = true)
data class Message(
        @PrimaryKey
        @Json(name = "id")
        var id: Int,
        @Json(name = "to")
        var recipients: List<User>?,
        @Json(name = "cc")
        var recipientsAsCC: List<User>?,
        @Json(name = "bcc")
        var recipientsAsBCC: List<User>?,
        @Json(name = "sender")
        var sender: User?,
        @Json(name = "sentAt")
        var sentAt: String?,
        @Json(name = "threadID")
        var threadID: String?,
        @Json(name = "inReplyTo")
        var inReplyTo: String?,
        @Json(name = "subjectPreview")
        var subjectPreview: String?,
        @Json(name = "subject")
        var subject: String?,
        @Json(name = "contentPreview")
        var contentPreview: String?,
        @Json(name = "content")
        var content: String?,
        @Json(name = "attachmentCount")
        var attachmentCount: Int,
        @Json(name = "deliveryStatus")
        var deliveryStatus: Int
)

@JsonClass(generateAdapter = true)
data class MessageResponse(
        @Json(name = "data")
        var data: List<Message>
)
