package com.dika.tesproofn.arch.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Created by Dika Putra on 28/02/19.
 * Contact: novianmahardikaputra@gmail.com
 * Project: TesProofn
 */

@JsonClass(generateAdapter = true)
data class Session(
        @Json(name = "token")
        var token: String,
        @Json(name = "user")
        var user: User
)