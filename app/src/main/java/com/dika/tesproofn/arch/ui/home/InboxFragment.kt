package com.dika.tesproofn.arch.ui.home

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.vm.MessageViewModel
import com.dika.tesproofn.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_inbox.*
import timber.log.Timber

class InboxFragment : BaseFragment<InboxFragment.OnInboxFragmentCallback>() {

    private lateinit var messageViewModel: MessageViewModel
    private val adapter = InboxAdapter()

    override fun setView(): Any {
        return R.layout.fragment_inbox
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        messages.layoutManager = LinearLayoutManager(activity as Context?, RecyclerView.VERTICAL, false)
        messages.setHasFixedSize(true)
        messages.adapter = adapter

        messageViewModel = ViewModelProviders.of(this, factory).get(MessageViewModel::class.java)
        messageViewModel.messages.observe(this, Observer {
            Timber.d(it.toString())
            adapter.addAll(it)
        })
        messageViewModel.error.observe(this, Observer {
            (activity as HomeActivity).showError(it)
        })
    }

    override fun onResume() {
        super.onResume()
        messageViewModel.getMessages()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.inbox_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_add -> (activity as HomeActivity).showError("Coming Soon")
        }
        return super.onOptionsItemSelected(item)
    }

    interface OnInboxFragmentCallback : BaseFragmentCallback
}