package com.dika.tesproofn.arch.ui.home

import android.content.Context
import android.view.View
import com.dika.tesproofn.BuildConfig
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.base.BaseAdapter
import com.dika.tesproofn.base.BaseViewHolder
import com.dika.tesproofn.utils.GlideUtils
import kotlinx.android.synthetic.main.item_message.view.*
import java.text.SimpleDateFormat
import java.util.*

class InboxViewHolder(private val context: Context, itemView: View) : BaseViewHolder<Message>(itemView) {
    override fun onBind(pos: Int, count: Int, item: Message, callback: BaseAdapter.ItemCallback<Message>?) {
        itemView.message_content_lbl.text = item.contentPreview
        itemView.message_topic_lbl.text = item.subjectPreview
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        val df2 = SimpleDateFormat("d MMM", Locale.getDefault())
        itemView.message_date_lbl.text = df2.format(df.parse(item.sentAt))
        item.sender?.let {
            itemView.message_email_lbl.text = item.sender?.email
            GlideUtils(context).show(
                    BuildConfig.BASE_URL + item.sender?.avatarSmall as String,
                    itemView.message_user_img,
                    R.color.colorPrimary,
                    R.color.colorPrimary
            )
        }
    }
}