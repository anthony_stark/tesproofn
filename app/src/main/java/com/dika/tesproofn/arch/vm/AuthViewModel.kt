package com.dika.tesproofn.arch.vm

import androidx.annotation.WorkerThread
import com.dika.tesproofn.arch.model.Session
import com.dika.tesproofn.arch.model.User
import com.dika.tesproofn.arch.ui.home.HomeActivity
import com.dika.tesproofn.base.BaseViewModel
import com.dika.tesproofn.base.MyApp
import com.dika.tesproofn.db.AppDatabase
import com.dika.tesproofn.network.AuthAPI
import com.dika.tesproofn.utils.Coroutines
import com.dika.tesproofn.utils.ErrorParser
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class AuthViewModel @Inject constructor() : BaseViewModel() {
    @Inject
    lateinit var authAPI: AuthAPI
    @Inject
    lateinit var db: AppDatabase

    private var subscription: Disposable? = null

    fun login(identifier: String, password: String) {
        subscription = authAPI.login(identifier, password)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { showLoading() }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { hideLoading() }
                .subscribe(
                        { onLoginSuccess(it) },
                        {
                            Timber.e(it)
                            showError(ErrorParser.getErrorMessage(it))
                        }
                )
    }

    override fun onCleared() {
        super.onCleared()
        subscription?.dispose()
    }

    private fun onLoginSuccess(session: Session) {
        Timber.d(session.toString())
        MyApp.instance.saveSession(session)
        Coroutines.io {
            insertUser(session.user)
        }
        movePage(HomeActivity::class)
    }

    @WorkerThread
    private fun insertUser(user: User) {
        db.userDAO().clear()
        db.userDAO().insert(user)
    }
}