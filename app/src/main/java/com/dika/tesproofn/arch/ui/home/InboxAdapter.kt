package com.dika.tesproofn.arch.ui.home

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.base.BaseAdapter

/**
 * Created by Dika Putra on 01/03/19.
 * Contact: novianmahardikaputra@gmail.com
 * Project: TesProofn
 */

class InboxAdapter : BaseAdapter<Message>() {
    override fun setView(viewType: Int): Int {
        return R.layout.item_message
    }

    override fun itemViewHolder(context: Context, view: View, viewType: Int): RecyclerView.ViewHolder {
        return InboxViewHolder(context, view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (holder is InboxViewHolder) {
            holder.onBind(position, data.size, get(position), itemCallback)
        }
    }

}