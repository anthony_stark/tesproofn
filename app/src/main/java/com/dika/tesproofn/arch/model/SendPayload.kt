package com.dika.tesproofn.arch.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity
data class SendPayload(
        @PrimaryKey(autoGenerate = true)
        @Json(name = "id")
        var id: Int,
        @Json(name = "subject")
        var subject: String,
        @Json(name = "content")
        var content: String,
        @Json(name = "to")
        var recipients: List<User>? = null,
        @Json(name = "cc")
        var recipientsAsCC: List<User>? = null,
        @Json(name = "bcc")
        var recipientsAsBCC: List<User>? = null,
        @Json(name = "content")
        var contentPreview: String? = null
)