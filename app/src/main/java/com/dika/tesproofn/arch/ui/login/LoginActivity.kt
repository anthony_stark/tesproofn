package com.dika.tesproofn.arch.ui.login

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.ui.home.HomeActivity
import com.dika.tesproofn.arch.vm.AuthViewModel
import com.dika.tesproofn.base.BaseActivity
import com.dika.tesproofn.base.MyApp
import com.dika.tesproofn.utils.LoadingUtils
import com.dika.tesproofn.utils.SnackBarUtils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {
    private lateinit var authViewModel: AuthViewModel
    private var loading: ProgressDialog? = null

    override fun setView(): Int {
        return R.layout.activity_login
    }

    override fun initView(savedInstanceState: Bundle?) {
        authViewModel = ViewModelProviders.of(this, factory).get(AuthViewModel::class.java)
        authViewModel.page.observe(this, Observer {
            startActivity(it.first, true)
        })
        authViewModel.error.observe(this, Observer {
            SnackBarUtils.showSnack(root_view, it)
        })
        authViewModel.loading.observe(this, Observer {
            if (it == View.VISIBLE) {
                if (loading == null) {
                    loading = LoadingUtils.simpleLoading(this).show()
                } else {
                    loading?.show()
                }
            } else {
                loading?.dismiss()
            }
        })

        login_btn.setOnClickListener {
            if (!validate()) return@setOnClickListener
            authViewModel.login(username_txt.text.toString(), password_txt.text.toString())
        }
    }

    override fun onResume() {
        super.onResume()
        if (MyApp.instance.isLogin()) {
            startActivity(HomeActivity::class, true)
        }
    }

    private fun validate(): Boolean {
        username_txt.error = null
        password_txt.error = null
        if (username_txt.text.isNullOrEmpty()) {
            username_txt.error = getString(R.string.empty_field)
            return false
        }
        if (password_txt.text.isNullOrEmpty()) {
            password_txt.error = getString(R.string.empty_field)
            return false
        }
        return true
    }
}