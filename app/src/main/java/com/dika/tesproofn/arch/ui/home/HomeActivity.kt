package com.dika.tesproofn.arch.ui.home

import android.os.Bundle
import android.view.MenuItem
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.ui.profile.ProfileFragment
import com.dika.tesproofn.base.BaseActivity
import com.dika.tesproofn.utils.SnackBarUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*

open class HomeActivity : BaseActivity(), InboxFragment.OnInboxFragmentCallback {
    override fun setView(): Int {
        return R.layout.activity_home
    }

    override fun initView(savedInstanceState: Bundle?) {
        toolbar.title = "Inbox"
        setSupportActionBar(toolbar)
        switchView(InboxFragment())
        bottomNav.setOnNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.nav_inbox -> {
                        toolbar.title = "Inbox"
                        switchView(InboxFragment())
                    }
                    R.id.nav_profile -> {
                        toolbar.title = "Profile"
                        switchView(ProfileFragment())
                    }
                    else -> throw  IllegalStateException("id not found")
                }
                return true
            }
        })
    }

    open fun showError(msg: String) {
        SnackBarUtils.showSnack(root_view, msg)
    }
}