package com.dika.tesproofn.arch.ui.profile

import android.os.Bundle
import android.view.View
import com.dika.tesproofn.R
import com.dika.tesproofn.base.BaseFragment

class ProfileFragment : BaseFragment<ProfileFragment.OnProfileFragmentCallback>() {
    override fun setView(): Any {
        return R.layout.fragment_empty
    }

    override fun initView(view: View, savedInstanceState: Bundle?) {

    }

    interface OnProfileFragmentCallback : BaseFragmentCallback
}