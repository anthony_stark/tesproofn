package com.dika.tesproofn.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

object SnackBarUtils {
    fun showSnack(view: View, msg: String): Snackbar {
        val snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
        snackbar.show()
        return snackbar
    }
}