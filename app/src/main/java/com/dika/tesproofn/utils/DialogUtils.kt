package com.dika.tesproofn.utils

import android.content.Context
import android.content.DialogInterface
import android.view.View
import androidx.appcompat.app.AlertDialog

class DialogUtils(private var appContext: Context) {

    fun showCompleteDialog(
            context: Context,
            view: View?,
            message: String,
            positif: String,
            negative: String,
            neutral: String,
            listener: DialogCompleteCallback?
    ) {
        val builder = AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(positif) { dialogInterface, _ ->
                    listener?.onConfirmed(
                            dialogInterface,
                            DialogInterface.BUTTON_POSITIVE
                    )
                }
                .setNegativeButton(negative) { dialogInterface, _ ->
                    listener?.onConfirmed(
                            dialogInterface,
                            DialogInterface.BUTTON_NEGATIVE
                    )
                }
                .setNeutralButton(neutral) { dialogInterface, _ ->
                    listener?.onConfirmed(
                            dialogInterface,
                            DialogInterface.BUTTON_NEUTRAL
                    )
                }
        builder.setView(view)
        builder.show()
    }

    fun showConfirmDialog(
            context: Context,
            view: View?,
            message: String,
            yes: String,
            no: String,
            listener: DialogConfirmationCallback?
    ) {
        val builder = AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(yes) { dialogInterface, _ -> listener?.onConfirmed(dialogInterface, true) }
                .setNegativeButton(no) { dialogInterface, _ -> listener?.onConfirmed(dialogInterface, false) }
        builder.setView(view)
        builder.show()
    }

    fun showInfoDialog(view: View?, message: String, listener: DialogConfirmationCallback?) {
        val builder = AlertDialog.Builder(appContext)
                .setView(view)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok) { dialogInterface, _ ->
                    listener?.onConfirmed(
                            dialogInterface,
                            true
                    )
                }
        builder.show()
    }

    interface DialogConfirmationCallback {
        fun onConfirmed(dialog: DialogInterface, isYes: Boolean)
    }

    interface DialogCompleteCallback {
        fun onConfirmed(dialog: DialogInterface, which: Int)
    }
}