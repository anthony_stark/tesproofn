package com.dika.tesproofn.utils

import android.content.Context
import android.widget.ImageView

class GlideUtils(private var context: Context) {
    fun show(source: Any, view: ImageView, placeholder: Int = 0, error: Int = 0) {
        val e: Int = if (error == 0) placeholder else error
        GlideApp.with(context)
                .load(source)
                .placeholder(placeholder)
                .error(e)
                .into(view)
    }
}