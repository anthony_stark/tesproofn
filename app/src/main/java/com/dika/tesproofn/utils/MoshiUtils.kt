package com.dika.tesproofn.utils

import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import timber.log.Timber

object MoshiUtils {
    fun toClass(json: String, clazz: Class<*>, isList: Boolean = false): Any? {
        try {
            var jsonAdapter = Moshi.Builder().build().adapter<Any>(clazz)
            if (isList) {
                val type = Types.newParameterizedType(List::class.java, clazz)
                jsonAdapter = Moshi.Builder().build().adapter<Any>(type)
            }
            return jsonAdapter.fromJson(json)
        } catch (e: Exception) {
            Timber.e(e)
        }
        return null
    }

    fun toJson(data: Any, clazz: Class<*>): String? {
        var jsonAdapter = Moshi.Builder().build().adapter<Any>(clazz)
        if (data is List<*>) {
            val type = Types.newParameterizedType(List::class.java, clazz)
            jsonAdapter = Moshi.Builder().build().adapter<Any>(type)
        }
        return jsonAdapter.toJson(data)
    }

}