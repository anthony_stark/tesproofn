package com.dika.tesproofn.utils.retrofit

import android.app.Application
import com.dika.tesproofn.base.MyApp
import com.dika.tesproofn.constant.Constant
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(private val context: Application) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        val token = MyApp.instance.getToken()
        if (token.isEmpty()) {
            MyApp.instance.logout()
            return null
        }
        if (response.request().header(Constant.KEY_HEADER_ACCESS_TOKEN) != token) {
            Timber.d("authenticator: use default token")
            return response.request().newBuilder()
                    .header(Constant.KEY_HEADER_ACCESS_TOKEN, "Bearer " + token)
                    .build()
        }

        Timber.d("authenticator response code: " + response.code())

//        if (response.code() == 401) {
//            Timber.d("authenticator: Token expired, request new user token")
//            MyApp.instance.logout()
//            Toast.makeText(context, "Sesi anda telah berakhir, mohon login ulang", Toast.LENGTH_LONG).show()
//        }
        return null
    }
}