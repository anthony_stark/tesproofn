package com.dika.tesproofn.utils.retrofit

import com.dika.tesproofn.constant.Constant
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import timber.log.Timber
import java.util.concurrent.TimeUnit


/**
 * Created by Dika Putra on 24/12/18.
 * Contact: novianmahardikaputra@gmail.com
 * Project: PSCKediri
 */

class OnlineCacheInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        Timber.d("okhttp: enable network cache")

        val originalResponse = chain.proceed(chain.request())

        val cacheControl = CacheControl.Builder()
                .maxAge(2, TimeUnit.MINUTES)
                .build()

        return originalResponse.newBuilder()
                .header(Constant.KEY_HEADER_CACHE_CONTROL, cacheControl.toString())
                .build()
    }
}