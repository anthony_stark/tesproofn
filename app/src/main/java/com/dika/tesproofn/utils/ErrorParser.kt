package com.dika.tesproofn.utils

import org.json.JSONObject
import retrofit2.HttpException
import timber.log.Timber
import java.net.HttpURLConnection

object ErrorParser {
    fun getErrorCode(e: Throwable?): Int {
        if (e == null) {
            return 0
        }
        if (e is HttpException) {
            Timber.d("throwable is HttpException, code: " + e.code())
            return e.code()
        }
        return 0
    }

    fun getErrorMessage(e: Throwable?): String {
        if (e == null) {
            return "Unknown error"
        }
        if (e is HttpException) {
            Timber.d("Http error code: " + e.code())
            return when (e.code()) {
                HttpURLConnection.HTTP_NOT_FOUND -> "Halaman tidak ditemukan"
                HttpURLConnection.HTTP_SERVER_ERROR -> "Terjadi kesalahan server, mohon coba beberapa saat lagi"
                401 -> "Username atau password tidak ditemukan, mohon periksa kembali"
                403 -> "Akun belum terverifikasi, periksa email Anda untuk verifikasi"
                404 -> "Anda belum terdaftar"
                else -> {
                    val responseBody = e.response().errorBody()
                    return try {
                        val jsonObject = JSONObject(responseBody?.string())
                        jsonObject.getString("message")
                    } catch (e: Exception) {
                        e.message ?: "No respon message"
                    }
                }
            }
        } else {
            Timber.d("Non Http error code: " + e.localizedMessage)
        }
        return e.localizedMessage
    }
}