package com.dika.tesproofn.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dika.tesproofn.BuildConfig
import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.arch.model.User
import com.dika.tesproofn.db.dao.MessageDAO
import com.dika.tesproofn.db.dao.UserDAO

@Database(entities = [User::class, Message::class], version = BuildConfig.DB_VERSION, exportSchema = false)
@TypeConverters(DataTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDAO(): UserDAO
    abstract fun messageDAO(): MessageDAO
}