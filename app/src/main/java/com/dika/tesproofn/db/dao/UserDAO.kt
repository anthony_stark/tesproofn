package com.dika.tesproofn.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.dika.tesproofn.arch.model.User
import com.dika.tesproofn.base.BaseDao

/**
 * Created by Dika Putra on 01/03/19.
 * Contact: novianmahardikaputra@gmail.com
 * Project: TesProofn
 */

@Dao
interface UserDAO : BaseDao<User> {
    @get:Query("select * from user limit 1")
    val get: User

    @Query("delete from user")
    fun clear()
}