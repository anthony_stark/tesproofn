package com.dika.tesproofn.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.dika.tesproofn.arch.model.Message
import com.dika.tesproofn.base.BaseDao

@Dao
interface MessageDAO : BaseDao<Message> {
    @Query("select * from message where :id = 1 limit 1")
    fun get(id: Int): Message

    @Query("select * from message")
    fun gets(): List<Message>

    @Query("delete from message")
    fun clear()
}