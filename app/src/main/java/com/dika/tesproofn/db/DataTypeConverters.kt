package com.dika.tesproofn.db

import androidx.room.TypeConverter
import com.dika.tesproofn.arch.model.User
import com.dika.tesproofn.utils.MoshiUtils

class DataTypeConverters {
    @TypeConverter
    fun fromRecipients(value: List<User>): String {
        return MoshiUtils.toJson(value, User::class.java) as String
    }

    @TypeConverter
    fun toRecipients(value: String): List<User> {
        return MoshiUtils.toClass(value, User::class.java, true) as List<User>
    }

    @TypeConverter
    fun fromListString(value: List<String>): String {
        return MoshiUtils.toJson(value, String::class.java) as String
    }

    @TypeConverter
    fun toListString(value: String): List<String> {
        return MoshiUtils.toClass(value, String::class.java, true) as List<String>
    }

    @TypeConverter
    fun fromUser(value: User): String {
        return MoshiUtils.toJson(value, User::class.java) as String
    }

    @TypeConverter
    fun toUser(value: String): User {
        return MoshiUtils.toClass(value, User::class.java) as User
    }
}