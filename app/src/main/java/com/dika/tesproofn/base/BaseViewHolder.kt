package com.dika.tesproofn.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun onBind(pos: Int, count: Int, item: T, callback: BaseAdapter.ItemCallback<T>?)
}