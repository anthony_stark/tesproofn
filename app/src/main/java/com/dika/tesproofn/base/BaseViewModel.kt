package com.dika.tesproofn.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.reflect.KClass

abstract class BaseViewModel : ViewModel() {
    open val loading: MutableLiveData<Int> = MutableLiveData()
    open val error: MutableLiveData<String> = MutableLiveData()
    open var page: MutableLiveData<Pair<KClass<*>, Bundle?>> = MutableLiveData()

    protected fun showError(msg: String) {
        error.value = msg
    }

    protected fun showLoading() {
        loading.value = View.VISIBLE
    }

    protected fun hideLoading() {
        loading.value = View.GONE
    }

    protected fun movePage(kClass: KClass<*>, bundle: Bundle? = null) {
        page.value = Pair(kClass, bundle)
    }
}