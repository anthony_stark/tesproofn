package com.dika.tesproofn.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
abstract class BaseFragment<T : BaseFragment.BaseFragmentCallback> : Fragment() {
    @Inject
    lateinit var factory: ViewModelProvider.Factory

    interface BaseFragmentCallback

    protected var callback: T? = null

    abstract fun setView(): Any

    abstract fun initView(view: View, savedInstanceState: Bundle?)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = setView()
        if (view is View) {
            return view
        } else if (view is Int) {
            return inflater.inflate(view, container, false)
        }
        return null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        if (context is BaseFragmentCallback) {
            callback = context as T
        } else {
            throw RuntimeException("$context must implement FragmentCallback inherit from BaseFragmentCallback")
        }
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }
}