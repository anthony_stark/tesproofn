package com.dika.tesproofn.base

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Context
import androidx.fragment.app.Fragment
import androidx.multidex.MultiDex
import com.dika.tesproofn.BuildConfig
import com.dika.tesproofn.R
import com.dika.tesproofn.arch.model.Session
import com.dika.tesproofn.arch.model.User
import com.dika.tesproofn.di.component.AppComponent
import com.dika.tesproofn.di.component.DaggerAppComponent
import com.dika.tesproofn.di.module.DatabaseModule
import com.dika.tesproofn.di.module.NetworkModule
import com.dika.tesproofn.di.module.UtilsModule
import com.dika.tesproofn.utils.DebugTree
import com.dika.tesproofn.utils.MoshiUtils
import com.dika.tesproofn.utils.PreferenceUtils
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import dagger.android.support.HasSupportFragmentInjector
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import javax.inject.Inject

class MyApp : Application(), HasActivityInjector, HasSupportFragmentInjector, HasServiceInjector {
    @Inject
    lateinit var sp: PreferenceUtils

    companion object {
        @JvmStatic
        private lateinit var component: AppComponent
        lateinit var instance: MyApp
            private set
    }

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var serviceInjector: DispatchingAndroidInjector<Service>


    override fun activityInjector(): AndroidInjector<Activity> {
        return androidInjector
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    override fun serviceInjector(): AndroidInjector<Service> {
        return serviceInjector
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        createComponent().inject(this)
        CalligraphyConfig.initDefault(
                CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.font_open_sans_regular))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        )
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree("proofn"))
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private fun createComponent(): AppComponent {
        component = DaggerAppComponent.builder()
                .application(this)
                .network(NetworkModule)
                .database(DatabaseModule)
                .utils(UtilsModule(this))
                .build()
        return component
    }

    fun saveSession(session: Session) {
        val user = MoshiUtils.toJson(session.user, User::class.java)
        sp.set("token", session.token)
        sp.set("user", user)
        sp.set("is_login", true)
    }

    fun getToken(): String {
        return sp.get(String::class, "token", "") as String
    }

    fun getUser(): User {
        val user = sp.get(String::class, "user") as String
        return MoshiUtils.toClass(user, User::class.java) as User
    }

    fun isLogin(): Boolean {
        return sp.get(Boolean::class, "is_login", false) as Boolean
    }

    fun logout() {
        sp.clear()
        //todo call login ui
    }
}