package com.dika.tesproofn.base

import androidx.room.*

@Dao
interface BaseDao<T> {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertIgnore(ts: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ts: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ts: List<T>): LongArray

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(ts: List<T>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(ts: List<T>): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(ts: T): Int

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateIgnore(ts: T): Int

    @Delete
    fun delete(ts: List<T>)

    @Delete
    fun delete(ts: T)
}
